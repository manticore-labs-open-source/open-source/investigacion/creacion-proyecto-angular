# Creación proyecto angular

Aquí se muestra cómo crear un proyecto angular.

## AngularJS

Es un framework para aplicaciones web desarrollado en TypeScript creado por Google.

### Requerimientos 

* NodeJS v8.9.0 o superior.
* NPM v5.5.0 o superior.

## Instalación

Se debe instalar angular con el siguiente comando.

``` Typescript
npm install @angular/cli -g
```

Una vez el comando termine, ejecute:

``` Typescript
ng -v
```
 ![](imagenes/cli.png)


Este comando imprimirá las versiones existentes. 

## Creación proyecto angular

Para este tutorial crearemos un proyecto "angular-prueba" 

``` Typescript
ng new nombre-app
```

Para correr nuestro proyecto, debemos ingresar al directorio creado con el nombre de nuestro proyecto y correr el siguiente comando 
```
ng s
```

 ![](imagenes/run.png)


La aplicación comenzara en el puerto **4200**

Abra cualquier explorador e ingrese la siguiente URL.
```
http://localhost:4200/
```
Finalmente se mostrará la siguiente pantalla. 

 ![](imagenes/pag.png)

<a href="https://twitter.com/alimonse29" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://3.bp.blogspot.com/-E4jytrbmLbY/XCrI2Xd_hUI/AAAAAAAAIAo/qXt-bJg1UpMZmTjCJymxWEOGXWEQ2mv3ACLcBGAs/s1600/twitter.png" title="Sígueme en Twitter"/> @alimonse29 </a><br>

